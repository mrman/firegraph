/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var pickFiles = require('broccoli-static-compiler');
var mergeTrees = require('broccoli-merge-trees');

var app = new EmberApp({
  wrapInEval: false,
  fingerprint: {
    exclude: ['icons/firegraph-logo.128px.png', 'icons/firegraph-logo.512px.png']
  }
});

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.

app.import('bower_components/pure/pure-min.css');
app.import('bower_components/pure/grids-responsive-min.css');
app.import('bower_components/localforage/dist/localforage.min.js');
app.import('bower_components/lodash/lodash.min.js');
app.import('bower_components/building-blocks/fonts.css');
app.import('bower_components/building-blocks/style/headers.css');
app.import('bower_components/building-blocks/style/icons.css');
app.import('bower_components/building-blocks/util.css');
app.import('bower_components/gaia-icons/gaia-icons-embedded.css');

var fontAwesomeFonts = pickFiles('bower_components/fontawesome', {
  srcDir: '/',
  destDir: 'assets/vendor/fontawesome'
});

var buildingBlockFonts = pickFiles('bower_components/building-blocks', {
  srcDir: 'fonts',
  destDir: 'assets/fonts'
});

var moveManifests = pickFiles('manifests', {
  srcDir: '/',
  files: ['manifest.webapp'],
  destDir: '/'
});

module.exports = mergeTrees([app.toTree(), moveManifests, buildingBlockFonts, fontAwesomeFonts]);
