import Ember from 'ember';
import _ from '../helpers/lodash';

export default Ember.Mixin.create({
  initialDataLoaded: null,

  /**
   * Do work after the intial data is loaded
   *
   * @param {function} fn - A function that accepts 2 parameters (resolve and reject
   * @returns A Promise that evaluates after the initial data is loaded
   */
  afterInitialDataIsLoaded: function(fn) {

    var initialDataLoadedPromise = this.get('initialDataLoaded');
    if (_.isNull(initialDataLoadedPromise)) {
      throw new Error("initialDataLoaded expected to be a promise at this point");
    }

    return new Ember.RSVP.Promise(function(resolve, reject) {
      // Ensure that the object has finished loading initial data before doing work
      initialDataLoadedPromise
        .then(function() { fn(resolve, reject); })
        .catch(reject);
    });

  }
});
