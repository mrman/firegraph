import Ember from 'ember';
import _ from '../helpers/lodash';

export default Ember.View.extend({
  click: function(e) {
    // If the rhs content is clicked and the left sidebar is shown, hide it (except for toggling buttons)
    let sidebarSvc = this.get('controller').sidebarSvc;
    let isShown = sidebarSvc.get('sidebarShown');
    let targetTogglesSidebar = _.has(e, 'target.dataset.togglesSidebar');

    if (isShown && !targetTogglesSidebar) {
      sidebarSvc.set('sidebarShown', false);
    }

    return false;
  }
});
