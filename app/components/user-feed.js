import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    loadMoreMedia: function() {
      this.instagramSvc
        .loadOlderFeedMedia()
        .catch(function() { alert("Failed to retrieve find older images from Instagram API!"); });
    }
  }
});
