import Ember from 'ember';
import _ from '../helpers/lodash';

export default Ember.Component.extend({
  actions: {
    toggleSidebar: function() { this.sidebarSvc.toggleSidebar(); },
    showSidebar: function() { this.sidebarSvc.showSidebar(); },
    hideSidebar: function() { this.sidebarSvc.hideSidebar(); },
    loadNewestMedia: function() {
      this.instagramSvc
        .loadNewestMedia()
        .then(function(newImages) {
          if (_.isEmpty(newImages)) { alert("No new images found"); }
        })
        .catch(function(err) {
          alert("An error occurred fetching newest images!", err);
        });
    }
  }
});
