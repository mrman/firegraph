import Ember from 'ember';

export default Ember.Controller.extend({
  sidebarClass: function() {
    return this.sidebarSvc.get('sidebarShown') ? 'show-sidebar' : '';
  }.property('sidebarSvc.sidebarShown')
});
