import Ember from 'ember';
import _ from '../helpers/lodash';

const ACCESS_TOKEN_REGEX = /#access_token=([\d\w\.]+)&?/;

export default Ember.Route.extend({
  model: function() {
    var instagramSvc = this.instagramSvc;
    return new Ember.RSVP.Promise(function (resolve, reject) {
      var isAuthenticated =  instagramSvc.get('isAuthenticated');

      // Check if the user was just authenticated by implicit oauth
      var matches = location.hash.match(ACCESS_TOKEN_REGEX);
      // Parse out and save the access_token if present and save it
      if (!isAuthenticated && matches) {
        Ember.Logger.debug("[APPLICATION] Retrieved access token from location hash, Setting accessToken on instagramSvc...");
        instagramSvc.set('accessToken', matches[1]);
        instagramSvc.set('accessTokenSetDate', new Date());
      }

      // If the user is not authenticated and did not just complete authentication, they're truly not authenticated.
      if (!isAuthenticated && _.isNull(matches)) {
        resolve({isAuthenticated: false});
        return;
      }

      // If the user is authenticated, load the user
      instagramSvc
        .getCurrentUser()
        .then(function() {
          resolve({
            isAuthenticated: instagramSvc.get('isAuthenticated'),
            user: instagramSvc.get('user')
          });
        })
        .catch(function(err) {
          alert("Error: Login failed. Please close application and try again");
          reject(err);
          Ember.Logger.error("[ERROR] - Failed to fetch current user!", err);
        });
    });

  },

  actions: {
    error: function(reason) {
      alert("Error: Internal app routing failure. Please reload (or reinstall) application and try again");
      Ember.Logger.error("[ERROR] Routing failure in main application route:", reason);

      // Reset location.hash if populated
      if (location.hash.match(ACCESS_TOKEN_REGEX)) {
        Ember.Logger.debug("Hash contains access token, wiping...");
        location.hash = "";
      }

      this.transitionTo('login');
    },

    closeSidebarThenTransition: function() {
      this.sidebarSvc.hideSidebar();
      this.transitionToRoute.apply(this, arguments);
    },

    logout: function() {
      this.sidebarSvc.hideSidebar();
      this.instagramSvc.logout();
      this.transitionTo('login');
    }
  }
});
