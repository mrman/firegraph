import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    var route = this;
    var instagramSvc = route.instagramSvc;

    // Check if user is authenticated
    return new Ember.RSVP.Promise(function(resolve, reject) {

      var isAuthenticated =  instagramSvc.get('isAuthenticated');

      // If user is not already authenticated, check for auth token and possibly redirect
      if (!isAuthenticated) {
        route.transitionTo('login');
        return;
      }

      // Load user information
      instagramSvc
        .getCurrentUser()
        .then(function() {

          // Load user's feed also
          instagramSvc
            .getCurrentUserFeed()
            .then(function() {
              resolve({
                isAuthenticated: instagramSvc.get('isAuthenticated'),
                user: instagramSvc.get('user'),
                userFeed: instagramSvc.get('userFeed')
              });
            })
            .catch(function(err) {
              alert("Error: Failed to load the current user's feed... Please close application and try again");
              Ember.Logger.error("[ERROR] - Failed to get the user's current feed", err);
              resolve({user: instagramSvc.get('user'), currentUseFeed: [], error: "Failed to get current user's feed"});
            });

        }).catch(function(err) {
          alert("Error: Failed to fetch current user's information. Please close application and try again");
          reject(err);
          Ember.Logger.error("[ERROR] - Failed to fetch current user!", err);
        });

    });
  },

  actions: {
    error: function(reason) {
      Ember.Logger.error("[ERROR] Routing failure in index route:", reason);
      this.transitionTo('login');
    }
  }
});
