import Ember from 'ember';

var IMPLICIT_AUTH_ENDPOINT = 'https://instagram.com/oauth/authorize/?response_type=token&redirect_uri=http://firegraph.vadosware.io&';

export default Ember.Route.extend({
  setupController: function(controller) {
    controller.set('loginUrl', this.instagramSvc.urlWithClientID(IMPLICIT_AUTH_ENDPOINT));
  }
});
