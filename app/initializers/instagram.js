import Instagram from '../services/instagram';

export default {
  name: 'instagramSvc',
  initialize: function(container, app) {
    app.register('services:instagramSvc', Instagram, {singleton: true});
    app.inject('route', 'instagramSvc', 'services:instagramSvc');
    app.inject('controller', 'instagramSvc', 'services:instagramSvc');
    app.inject('component', 'instagramSvc', 'services:instagramSvc');
  }
};
