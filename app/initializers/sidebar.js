import SidebarService from '../services/sidebar';

export default {
  name: 'sidebarSvc',
  initialize: function(container, app) {
    app.register('services:sidebarSvc', SidebarService, {singleton: true});
    app.inject('route', 'sidebarSvc', 'services:sidebarSvc');
    app.inject('controller', 'sidebarSvc', 'services:sidebarSvc');
    app.inject('component', 'sidebarSvc', 'services:sidebarSvc');
  }
};
