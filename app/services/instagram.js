import Ember from 'ember';
import localforage from '../helpers/localforage';
import _ from '../helpers/lodash';
import InitialDataIncluded from '../helpers/initial-data-loaded';

const LOCAL_STORAGE_DATA_KEY = 'instagram-service-data';
const INSTA_API = 'https://api.instagram.com/v1/';
const CLIENT_ID = 'a0dcac18d0ec42e7a25d8a48c5f9bbca';
const FEED_MEDIA_COUNT = 7;
const XHR_CREATION_FN = () => new XMLHttpRequest({mozSystem: true});

/**
 * Do Ajax call with jquery, splicing in necessary elements for CORS request on FFOS
 *
 * @param {string} url - The URL to which to perform the request
 * @param {Object} settings - The settings to apply to the request
 * @returns A jQuery HTTP request object
 */

function doAjax(settings={}) {
  settings = _.extend(settings, {xhr: XHR_CREATION_FN});
  return Ember.$.ajax(settings);
}

export default Ember.Service.extend(InitialDataIncluded, {
  /**
   * InstagramService, manages the requests/data that comes from the Instagram API
   * @exports InstagramService
   */

  accessToken: null,
  accessTokenSetDate: null,
  user: null,
  userFeed: Ember.A([]), // userFeed[0] is newest
  userFeedIDLookup: function() {
    return _.countBy(this.get('userFeed'), 'id');
  }.observes('userFeed.@each.id'),

  /**
   * Utility function for easily adding the current access token to a request URL
   *
   * @param {string} url - the url to append to
   * @returns The fed in URL, appended with "?access_token=ACCESS-TOKEN"
   */
  urlWithAccessToken: function(url) { return `${url}access_token=${this.get('accessToken')}`; },

  /**
   * Utility function for easily adding the current access token to a request URL
   *
   * @param {string} url - the url to append to
   * @returns The fed in URL, appended with "?access_token=ACCESS-TOKEN"
   */
  urlWithClientID: function(url) { return `${url}client_id=${CLIENT_ID}`; },


  /**
   * Get the current user (either from object-local cache or from Instagram's API)
   *
   * @returns A Promise which resolves to the current user information
   */
  getCurrentUser: function() {
    var svc = this;

    Ember.Logger.debug("[INSTAGRAM SVC] Attempting to retrieve current user");
    return svc.afterInitialDataIsLoaded(function(resolve, reject) {
      var currentUser = svc.get('user');
      Ember.Logger.debug("[INSTAGRAM SVC] During current user load, currentUser:", currentUser);

      // Exit early if we already have a cached value for user
      if (!_.isNull(currentUser)) {
        Ember.Logger.debug("[INSTAGRAM SVC] Returning cached current user:", currentUser);
        resolve(currentUser);
        return;
      }

      // Fetch the current user if there isn't one cached
      svc
        ._fetchCurrentUser()
        .then(function(resp) {
          if (_.has(resp, "meta.code") && resp.meta.code === 200) {
            let user = resp.data;
            Ember.Logger.debug("[INSTAGRAM SVC] Successfully retrieved current user:", user);
            svc.set('user', user);
            svc.backupToLocalStorage();
            resolve(user);
          } else {
            reject(new Error("[INSTAGRAM SVC] Request succeeded but meta indicates error"));
          }
        })
        .catch(function(err) {
          Ember.Logger.debug("[INSTAGRAM SVC] Failed to retrieve current user, error:", err);
          reject(err);
        });

    });
  },

  /**
   * Get the current uers's feed
   *
   * @param {boolean} forceFetch - Force getCurrentUserFeed to make a request (freshest data)
   */
  getCurrentUserFeed: function(forceFetch=false) {
    var svc = this;

    return svc.afterInitialDataIsLoaded(function(resolve, reject) {
      var currentUserFeed = svc.get('userFeed');
      var minId = _.isEmpty(currentUserFeed) ? 0 : _.last(currentUserFeed).id;

      // Exit early if it's ok to return the cached currentUserFeed
      if (!forceFetch && !_.isEmpty(currentUserFeed)) {
        resolve(currentUserFeed);
        return;
      }

      // Fetch user's feed
      svc
        ._fetchFeedMedia({minId: minId, count: FEED_MEDIA_COUNT})
        .then(function(resp) {
          if (_.has(resp, "meta.code") && resp.meta.code === 200) {
            let paginationInfo = resp.pagination;
            let media = resp.data;

            // Save latest pagination info
            svc.set('latestPaginationInfo', paginationInfo);

            // Save media items
            svc.userFeed.pushObjects(svc.filterFeedDuplicates(media));
            svc.backupToLocalStorage();

            resolve(svc.get('userFeed'));
          } else {
            reject(new Error("[INSTAGRAM SVC] Request succeeded but meta indicates error"));
          }
        })
        .catch(reject);

    });
  },


  /**
   * Computed property that returns whether the user is authenticated
   * (by checking whether the access token is non-null)
   *
   * @returns A Promise that will resolve to whether the user is authenticated or not
   */
  isAuthenticated: function() {
    return !_.isNull(this.get('accessToken'));
  }.property('accessToken'),


  /**
   * Load newest media (if available) from the Instagram API
   *
   * @returns A Promise that resolves to the new media (if any) that has been added to the feed
   */
  loadNewestMedia: function() {
    let svc = this;
    let newest = _.first(svc.get('userFeed')).id;

    return new Ember.RSVP.Promise(function(resolve, reject) {
      svc._fetchFeedMedia({minId: newest, count: FEED_MEDIA_COUNT})
        .then(function(resp) {
          if (_.has(resp, "meta.code") && resp.meta.code == 200) {
            let newerMedia = resp.data;

            // Filter out duplicates and add new (older) media to end of feed
            svc.userFeed.unshiftObjects( svc.filterFeedDuplicates(newerMedia) );
            svc.backupToLocalStorage();
            resolve(newerMedia);
          } else {
            reject(new Error("Instagram reported error retrieving new media"));
          }
        })
        .catch(reject);
    });
  },

  /**
   * Load more media (if available) from Instagram service into feed
   *
   * @returns A Promise that resolves to the new media (if any) that has been added to the feed
   */
  loadOlderFeedMedia: function() {
    let svc = this;
    let oldest = _.last(svc.get('userFeed')).id;

    return new Ember.RSVP.Promise(function(resolve, reject) {
      svc._fetchFeedMedia({maxId: oldest, count: FEED_MEDIA_COUNT})
        .then(function(olderMedia) {
          // Filter out duplicates and add new (older) media to end of feed
          svc.userFeed.pushObjects( svc.filterFeedDuplicates(olderMedia) );
          svc.backupToLocalStorage();
          resolve(olderMedia);
        })
        .catch(reject);
    });
  },

  /**
   * Filter out duplicates that are already in teh feed
   *
   * @param {list} media - List of media items to be filtered (items are expected to have an id attribute)
   * @returns the given list without any media items that already exist in the known feed
   */
  filterFeedDuplicates: function(media) {
    let knownIds = this.userFeedIDLookup();
    return _.filter(media, (m) => !_.has(knownIds, m.id));
  },

  /**
   * Fetch the current user's information from the Instagram API
   *
   * @returns a {@link Promise} that will resolve to data provided by the Instagram API, or null
   */
  _fetchCurrentUser: function() {
    var svc = this;

    return new Ember.RSVP.Promise(function(resolve, reject) {
      let accessToken = svc.get('accessToken');
      Ember.Logger.debug("[INSTAGRAM SVC] Attempting to fetch current user with access token:", accessToken);
      // Reject early and quit if accessToken is still null
      if (_.isNull(accessToken)) { reject(null); return; }

      let url = svc.urlWithAccessToken(INSTA_API + 'users/self?');
      doAjax({url})
        .then(resolve)
        .fail(reject);
    });
  },

  /**
   * Fetch media from the current user's feed from the Instagram API (images limited to 7 at a time)
   *
   * @param {string} options - Options to the Instagram API
   * @param {string} options.count - the number of media to return
   * @param {string} options.min_id - Return media later than this numeric id
   * @param {string} options.max_id - Return media earlier than this numeric ID
   * @returns a {@link Promise} that will resolve to data provided by the Instagram API, or null
   */
  _fetchFeedMedia: function(opts) {
    var svc = this;

    return new Ember.RSVP.Promise(function(resolve, reject) {
      // Reject early and quit if accessToken is still null
      if (_.isNull(svc.get('accessToken'))) { reject(null); return; }

      // Build the URL
      var url = INSTA_API + `users/self/feed?`;
      if (_.has(opts, 'count') && _.isNumber(opts.count)) { url += 'count=' + opts.count + '&'; }
      if (_.has(opts, 'minId')) { url += 'min_id=' + opts.minId + '&'; }
      if (_.has(opts, 'maxId')) { url += 'max_id=' + opts.maxId + '&'; }
      url = svc.urlWithAccessToken(url);

      setTimeout(() => {
        doAjax({url})
          .then(resolve)
          .fail(reject);
      }, 2000);
    });
  },

  /**
   * Logout of instagram service
   */
  logout: function() {
    this.set('user', null);
    this.set('accessToken', null);
    this.set('userFeed', []);
    this.set('isAuthenticated', false);
    this.clearLocalStorage();
  },

  /**
   * Back up this service's contents to local storage
   */
  backupToLocalStorage: function() {
    var svc = this;
    // Wait until initial data is loaded before backing up to local storage
    svc.get('initialDataLoaded')
      .then(function() {
        localforage.setItem(LOCAL_STORAGE_DATA_KEY, JSON.stringify({
          user: svc.get('user'),
          accessToken: svc.get('accessToken'),
          userFeed: svc.get('userFeed')
        }));
      });
  }.observes('accessToken', 'user', 'userFeed'),

  /**
   * Clear out saved data in localstorage
   */
  clearLocalStorage: function() {
    localforage.removeItem(LOCAL_STORAGE_DATA_KEY);
  },

  /**
   * Restore this service's contents to local storage
   *
   * @returns A Promise that evaluates to true or false based success/failure of loading from local storage
   */
  restoreFromLocalStorage: function() {
    var svc = this;

    return new Ember.RSVP.Promise(function(resolve, reject) {
      // Get the data from local storage
      localforage
      .getItem(LOCAL_STORAGE_DATA_KEY)
      .then(function(text) {
        var json = JSON.parse(text);
        // If there is no saved data to load, resolve immediately
        if (_.isUndefined(json) || _.isNull(json)) { resolve(true); return; }

        // Load user from localstorage, if present
        svc
          .set('user', _.get(json, 'user', null))
          .set('accessToken', _.get(json, 'accessToken', null));
        svc.userFeed.setObjects(_.get(json, 'userFeed', null));

        resolve(true);
      }).catch(function(err) {
        alert("An error occurred while retrieving saved Instagram service data");
        reject(err);
      });
    });
  },

  /**
   * Initialize the Instagram service, load data from localstorage if available
   */
  init: function() {
    var svc = this;

    // Load data from local storage asynchronously, fulfill initialDataLoaded promise
    svc.set('initialDataLoaded', svc.restoreFromLocalStorage());

    this._super.apply(this, arguments);
  }

});
