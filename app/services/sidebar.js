import Ember from 'ember';


export default Ember.Service.extend({
  /**
   * SidebarService, manages the sidebar
   * @exports SidebarService
   */

  sidebarShown: false,

  sidebarClass: function() {
    return this.get('sidebarShown') ? 'show-sidebar' : '';
  }.property('sidebarShown'),

  toggleSidebar: function() { this.set('sidebarShown', !this.get('sidebarShown')); },
  showSidebar: function() { this.set('sidebarShown', true); },
  hideSidebar: function() { this.set('sidebarShown', false); }
});
